import React from 'react';
import { Link } from 'react-router-dom';

export const Searchinside = ({handleShow}) => {
  return  <div>
  

  
  <div class="inside_navbar" id="nav-">
      <ul class="nav__list">
          
          <li class="nav__item">
              <a onClick={handleShow} class="insidenav__link">
                  <i class='bx bx-search nav__icon'></i>
                 
              </a>
          </li>
          
      </ul>
  </div>  


  
</div>;
};
