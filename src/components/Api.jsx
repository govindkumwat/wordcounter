import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
  useLocation,
  useParams
} from "react-router-dom";
import '../App.css'
import 'fa-icons'
import { LinkCircle } from "iconsax-react";
import { Chips, Chip } from "@mantine/core";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";
import Masonry from "react-masonry-css";
import useSWR from "swr";
import placehold from './placeholder.jpg'
import { Spinner, Placeholder } from "react-bootstrap";


const fetcher = (url) => fetch(url).then((r) => r.json());

export default function SimpleInfinateScroll() {
  return (
    <Router>
      <ModalSwitch />
    </Router>
  );
}

function ModalSwitch() {
  let location = useLocation();

  let background = location.state && location.state.background;


  return (
    <div>
      <Switch location={background || location}>
        <Route exact path="/img" children={<Gallery />} />
        <Route path="/img/:id" children={<ImageView />} />
      
      </Switch>

      {background && <Route path="/img/:id" children={<Modal />} />}
    </div>
  );
}

function Thumbnail({ color }) {
  return (
    <div
      style={{
        width: 50,
        height: 50,
        background: color
      }}
    />
  );
}

function Image({ color }) {
  let history = useHistory();
  return (
    <>
    <button className="closebutton" >CLose</button>
    <div className="model"
      style={{
        height: "100%",
        background: color
      }}
    >
        
      
      <img src = {color?.url}/>
      <p className="picwithtext">{color?.title}</p>

      <span className="subredditname">{color?.subreddit}</span>

      <a href={`http://www.reddit.com${color?.permalink}`} className="sourcecontainer">
        <LinkCircle className='redditsource' size="30" color="#ffffff"/>
        <p className="sourcetext">Source</p>
        </a>
          
    </div>
    <div>
      <p style = {{
        color: "white",
        background: "black",
        align: "bottom",
        padding: "10em",
      }}>
         
      </p>
    </div>
    </>
  );
}

function Gallery() {
  let location = useLocation();
  const [img, setImg] = useState([]);
  const [page, setPage] = useState(1);
  const iddata = img.slice(-1);
  const [filter, setFilter] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const baseUrl = "https://www.reddit.com/r";
  const subreddit = "pics";
  const filtered = (e) => {
    setFilter(e);
  };

  useEffect(async () => {
    subreddit &&
      await fetch(
        `${baseUrl}/${subreddit}.json?after=${
          iddata.length > 0 ? iddata.map((id) => `t3_${id.data.id}`) : ""
        }&page=${page}&limit=30`
      )
        .then((response) => response.json())
        .then((data) => {
          setImg((currentImage) => [...currentImage, ...data.data.children])
          setIsLoading(true);
        }     
        );
  }, [ page, filter]);

  const breakpointColumnsObj = {
    default: 3,
    1100: 2,
    700: 2,
    500: 2,
    400: 2
  };
  
  const scrollToEnd = () => {
    setPage(page + 1);
    
  };
  window.onscroll = function () {
    const bottom = document.documentElement.scrollHeight - window.innerHeight;
    const scrollposition = document.documentElement.scrollTop; 
    if (
   (bottom - scrollposition <= 0 )){
      scrollToEnd();

    }
  };
  
  return (
    <div>
     
      <div className="filters">
        <Chips>
          <Chip value="new" onClick={(e) => filtered(e.target.value)}>
            New
          </Chip>
          <Chip value="best" onClick={(e) => filtered(e.target.value)}>
            Best
          </Chip>
          <Chip value="rising" onClick={(e) => filtered(e.target.value)}>
            Rising
          </Chip>
          <Chip value="hot" onClick={(e) => filtered(e.target.value)}>
            Hot
          </Chip>
        </Chips>
      </div>

      <div className="col-lg-8 mx-auto">
        <Masonry
          breakpointCols={breakpointColumnsObj}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
        >
          {img.length > 1 &&
            img
              .filter((imgs) => imgs.data.post_hint === "image" )
              .map((imgs) => (
               
                <>
                  <div id="center">

               

                    <Link
                      key={imgs.data.id}
                      
                      to={{
                        pathname: `/img/${imgs.data.id}`,
                        state: { background: location },
                      }}
                     
                    >
                      <LazyLoadImage  
                        className="photolist "
                        src={ imgs.data.url_overridden_by_dest}
                        effect="black-and-white"
                        delayMethod="throttle"
                        alt={imgs.data.title}
                        loading="lazy"
                        threshold={100}
                        key={imgs.data.id}
                        fadeIn="true"
                        placeholder={<Placeholder as="div" animation="wave">
                        <Placeholder sm={12} />
                        <Spinner animation="border" variant="primary" />
                      </Placeholder>}
                      useIntersectionObserver="true"
                     
                       

                      ></LazyLoadImage>
                    </Link>
                  </div>
                </>
              ))}
        </Masonry>
         
      </div>
      {!isLoading && <div className="plzWait"> <Spinner animation="border" variant="primary" /> </div>}
    </div>
  );
}

function ImageView() {
  let { id } = useParams();
  const [detailphoto, setDetailphoto] = useState();
  useEffect(() => {
    fetch(`https://www.reddit.com/${id}.json`)
      .then((response) => response.json())
      .then((data) => setDetailphoto(data[0].data.children[0].data));
  }, [id]);


  return (
    <div>
      {<Image  color={detailphoto}  />}



    </div>
  );
}

function Modal() {
  let history = useHistory();
  let { id } = useParams();
 



 

  const {data} = useSWR(`https://www.reddit.com/${id}.json`, fetcher);
  
  if (!data) return <p className="postloader"><Spinner animation="border" variant="primary" /></p>;
  let back = (e) => {
    e.stopPropagation();
    history.goBack();
  };



  return (
    <div >
      <div className="model">
        <button class="closebutton"  onClick={back} className="closebutton">
         X
        </button>
        <img src={data && data[0].data.children[0].data.url_overridden_by_dest} />
        <p className="picwithtext">{data && data[0].data.children[0].data.title}</p>
        <span className="subredditname">{data && data[0].data.children[0].data.subreddit}</span> 
        <a href={`http://www.reddit.com${data && data[0].data.children[0].data.permalink}`} className="sourcecontainer">
        <LinkCircle className='redditsource' size="30" color="#ffffff"/>
        <p className="sourcetext">Source</p>
        </a>
      </div>
    </div>
  );
}
