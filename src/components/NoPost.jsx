import React from 'react';
import { GallerySlash } from 'iconsax-react';
import '../App.css'

export const NoPost = () => {
  return <div>
      <div className="emptyreddit"> 
      <GallerySlash size="32" color="black"/>
      <p>No Posts!</p></div>
  </div>;
};
