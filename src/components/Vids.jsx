import React, { useState, useEffect } from "react";
import placeit from './placeholder.jpg'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
  useLocation,
  useParams
} from "react-router-dom";
import '../App.css'
import 'fa-icons'
import { Chips, Chip } from "@mantine/core";
import "react-lazy-load-image-component/src/effects/blur.css";
import Masonry from "react-masonry-css";
import useSWR from "swr";
import ReactPlayer from 'react-player'
import { LinkCircle } from "iconsax-react";
import { Spinner } from "react-bootstrap";

const fetcher = (url) => fetch(url).then((r) => r.json());

export default function Vids() {
  return (
    <Router>
      <VideoSwitch />
    </Router>
  );
}

function VideoSwitch() {
  let location = useLocation();
  let background = location.state && location.state.background;



  return (
    <div>
      <Switch location={background || location}>
        <Route exact path="/videos" children={<VideoGallery />} />
        <Route path="/vids/:id" children={<VideoDetails />} />      
      </Switch>
      {background && <Route path="/vids/:id" children={<Modal />} />}
    </div>
  );
}
function Thumbnail({ color }) {
  return (
    <div
      style={{
        width: 50,
        height: 50,
        background: color
      }}
    />
  );
}

function Image({ color }) {
  let history = useHistory();
  return (
    <>
    <button className="closebutton" >Close</button>
    <div className="model"
      style={{
        height: "100%",
        background: color
      }}
    >
        
      
      <ReactPlayer playIcon  autoplay controls loop url={color?.media.reddit_video.fallback_url}/>
   
      <p className="picwithtext">{color?.title}</p>
      
      <span className="subredditname">{color?.subreddit}</span>

      <a href={`http://www.reddit.com${color?.permalink}`} className="sourcecontainer">
        <LinkCircle className='redditsource' size="30" color="#ffffff"/>
        <p className="sourcetext">Source</p>
        </a> 
 
    </div>
    <div>
      <p style = {{
        color: "white",
        background: "black",
        align: "bottom",
        padding: "10em",
      }}>Hello</p>
    </div>
    </>
  );
}

function VideoGallery() {
  let location = useLocation();
  const [img, setImg] = useState("");
  const [page, setPage] = useState(1);
  const iddata = img.slice(-1);
  const [filter, setFilter] = useState("");
  const [isLoading, setIsLoading] = useState(false);


  const baseUrl = "https://www.reddit.com/r";
  const subreddit = "gifsthatkeepongiving";

  const filtered = (e) => {
    setFilter(e);
  };

  useEffect(async () => {
    subreddit &&
      (await fetch(
        `${baseUrl}/${subreddit}/${filter}.json?after=${
          iddata.length > 0 ? iddata.map((id) => `t3_${id.data.id}`) : ""
        }&page=${page}&limit=20`
      )
        .then((response) => response.json())
        .then((data) =>
          setImg((currentImage) => [...currentImage, ...data.data.children])
         
        ));
        setIsLoading(true);
  }, [page]);

  useEffect(async () => {
    await fetch(
      `${baseUrl}/${subreddit}/${filter}.json?after=${
        iddata.length > 0 ? iddata.map((id) => `t3_${id.data.id}`) : ""
      }&page=${page}&limit=100`
    )
      .then((response) => response.json())
      .then((data) => setImg([...data.data.children]));
      setIsLoading(true);
  }, [filter]);



  const breakpointColumnsObj = {
    default: 3,
    1100: 2,
    700: 2,
    500: 2,
    400: 2
  };

  const scrollToEnd = () => {
    setPage(page + 1);
  };
  window.onscroll = function () {
    const bottom =
      Math.ceil(window.innerHeight + window.scrollY) >=
      document.documentElement.scrollHeight;

    if (
      (bottom,
      Math.ceil(window.innerHeight + window.scrollY) >=
        document.documentElement.scrollHeight)
    ) {
      scrollToEnd();
     
    }
  };



  return (
    <div>
      <div className="filters">
        <Chips>
          <Chip value="new" onClick={(e) => filtered(e.target.value)}>
            New
          </Chip>
          <Chip value="best" onClick={(e) => filtered(e.target.value)}>
            Best
          </Chip>
          <Chip value="rising" onClick={(e) => filtered(e.target.value)}>
            Rising
          </Chip>
          <Chip value="hot" onClick={(e) => filtered(e.target.value)}>
            Hot
          </Chip>
        </Chips>
      </div>

      <div className="col-lg-8 mx-auto">
        <Masonry
          breakpointCols={breakpointColumnsObj}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
        >
          {img.length > 1 &&
            img
              .filter((imgs) => imgs.data.post_hint === "hosted:video")
              .map((imgs) => (
                <>
                  <div id="center">
                    <Link
                      key={imgs.data.id}
                      to={{
                        pathname: `/vids/${imgs.data.id}`,
                        state: { background: location }
                      }}
                    >
                      {!imgs ? <Spinner animation="border" />  :  <video key={imgs.data.id} playing= 'true' poster={placeit} preload muted playIcon url={imgs.data.media.reddit_video?.fallback_url} />
                    }
                    </Link>
                  </div>
                </>
              ))}
        </Masonry>
      </div>
      
      {!isLoading && <div className="plzWait"> <Spinner animation="border" variant="primary" /> </div>}
    </div>
  );
}

function VideoDetails() {
  let { id } = useParams();
  const [detailphoto, setDetailphoto] = useState();
  useEffect(() => {
    fetch(`https://www.reddit.com/${id}.json`)
      .then((response) => response.json())
      .then((data) => setDetailphoto(data[0].data.children[0].data));
  }, [id]);




  return (
    <div>
      {<Image  color={detailphoto}  />}
  


    </div>
  );
}

function Modal() {
  let history = useHistory();
  let { id } = useParams();



  const {data} = useSWR(`https://www.reddit.com/${id}.json`, fetcher);
  

  if (!data) return <p>Loading...</p>;


  let back = (e) => {
    e.stopPropagation();
    history.goBack();
  };


  return (
    <div >
      <div className="model">
       
        <button class="closebutton"  onClick={back} className="closebutton">
         X
        </button>
        <video playing='true' autoplay="autoplay"  loop muted playIcon className="videolist" src={data &&  data[0].data.children[0].data.media.reddit_video?.fallback_url} />
        <p className="picwithtext">{data && data[0].data.children[0].data.title}</p>
        <span className="subredditname">{data && data[0].data.children[0].data.subreddit}</span> 
        <a href={`http://www.reddit.com${data && data[0].data.children[0].data.permalink}`} className="sourcecontainer">
        <LinkCircle className='redditsource' size="30" color="#ffffff"/>
        <p className="sourcetext">Source</p>
        </a>
       
      </div>
    </div>
  );
}
