import React, {useEffect} from 'react'
import millify from "millify";
import { Link } from 'react-router-dom';

import './newcss.css'

export const Redditheader = ({subreddit}) => {
    const [subredditData, setSubredditData] = React.useState(null)


    useEffect (async() => {
        const response = await fetch(`https://www.reddit.com/r/${subreddit}/about.json`);
        const data = await response.json();
        setSubredditData(data)
      
    }, [subreddit])

    function nFormatter(num) {
        if (num >= 1000000000) {
           return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'T';
        }
        if (num >= 1000000) {
           return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
        }
        if (num >= 1000) {
           return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
        }
        return num;
   }



    


    return (
       

        <>
        <div className='headcontainer'>
            <div className="profile">
                <img src={subredditData?.data.header_img ? subredditData?.data.header_img : 'https://www.iconpacks.net/icons/2/free-reddit-logo-icon-2436-thumb.png' } alt="" />
                <div className="information">
                    <span classname='title'>{subredditData?.data.display_name}</span> <br />
                    <a classname='description'>Subreddit</a> <br />

                    <div className="buttons">

                   

                    <Link to ='' className='sourcebutton'> <i class="fa fa-paper-plane" aria-hidden="true"></i> &nbsp; Source </Link>

                   

                    </div>
                   
                </div>
                
            </div>


            <div className="counts">

               

            

                <div className="totalfollowers">
                <span className='subscount'>{ nFormatter(subredditData?.data.subscribers)}</span>  <br />
                <p>Members</p>
                </div>

                <div className="activefollowers">
                <span className='subscount'>{nFormatter(subredditData?.data.active_user_count)}</span>  <br />
                <p>Online </p>
                </div>

                <div className="subtype">
                <span className='subscount'>{subredditData?.data.over18 ? 'NSFW' : 'SFW'}</span>  <br />
                <p>Sub Type</p>
                </div>
                </div>
        </div>

       
        
        </>
    )
}
