import { useState, useEffect, useReducer } from "react"
import '../App.css';
import { Link,BrowserRouter as Router, useParams, Switch, Route, useHistory, useLocation  } from "react-router-dom";
import { Chips, Chip } from '@mantine/core';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { GallerySlash } from "iconsax-react";
import { LinkCircle } from "iconsax-react";
import Masonry from "react-masonry-css";
import {Spinner} from 'react-bootstrap';
import  useSWR  from "swr";
import ReactPlayer from 'react-player'
import placeit from './placeholder.jpg'
import { Redditheader } from "./redditheader/Redditheader";
import { NoPost } from "./NoPost";


const fetcher = (url) => fetch(url).then((r) => r.json());

export default function SearchReddit() {

  return (
    <Router>
      <ModalSwitch />
    </Router>
  )
}

function ModalSwitch() {
  let location = useLocation();

  let background = location.state && location.state.background;



  return (
    <div>
      <Switch location={background || location}>
        <Route exact path="/r/:subreddit" children={<Gallery />} />
        <Route path="/searchreddit/:id" children={<ImageView />} />

      </Switch>

      {background && <Route path="/searchreddit/:id" children={<Modal />} />}
    </div>
  )
}

function Thumbnail({ color }) {
  return (
    <div
      style={{
        width: 50,
        height: 50,
        background: color
      }}
    />
  )
}

function Image({color}) {
  let history = useHistory();
  return (
    <>
    <button className="closebutton">
      Close
    </button>
    <div className="model"
      style={{
        height: "100%",
        background: color
      }}
    >
    
      {color && color.post_hint == 'image' ? (
        <img src={color?.url} alt="" />
      ) : (
        color && color.post_hint == 'hosted:video' ? (
           <ReactPlayer playIcon  autoplay controls loop url={color?.media.reddit_video.fallback_url}/>
        ) : (
          ''
        )
      )}
     
      <p className="picwithtext">{color?.title}</p>
      <span className="subredditname"> {color?.subreddit}</span>
      <a href={`http://www.reddit.com${color?.permalink}`} className="sourcecontainer">
      <LinkCircle className='redditsource' size="30" color="#ffffff"/>
        <p className="sourcetext">Source</p>
      </a>
    </div>

    <div>
      <p style = {{
        color: "white",
        background: "black",
        align: "bottom",
        padding: "10em",
      }}>Hello</p>
    </div>

  
    </>
  )
}

function Gallery (props) {


  let location = useLocation();
  const [img, setImg] = useState("");
  const [page, setPage] = useState(0);
  const iddata = img.slice(-1);
  const [filter, setFilter] = useState("hot");
  const [isLoading, setIsLoading] = useState(false);


  
  let {subreddit} = useParams();
  console.log(subreddit)


  

  const baseUrl = "https://www.reddit.com/r";
 
  
  const filtered = (e) => { 
    setFilter(e);
  };

  useEffect( () => {
    subreddit &&
       fetch(
        `${baseUrl}/${subreddit}/${filter}.json?after=${
          iddata.length > 0 ? iddata.map((id) => `t3_${id.data.id}`) : ""
        }&page=${page}&limit=60`
      )
        .then((response) => response.json())
        .then((data) => {
          setImg((currentImage) => [...currentImage, ...data.data.children], [...data.data.children])
          setIsLoading(true);
        }     
        );
  },[subreddit,page, filter]);


  const breakpointColumnsObj = {
    default: 3,
    1100: 2,
    700: 2,
    500: 2,
    400: 2
  };

  const scrollToEnd = () => {
    setPage(page + 1);
  };
  window.onscroll = function () {
    const bottom = document.documentElement.scrollHeight - window.innerHeight;
    const scrollposition = document.documentElement.scrollTop; 
    if (
      (bottom - scrollposition <= 0) &&
      isLoading
    ) {
      scrollToEnd();
      
    }
  };

  return (
    <>
    <div>
        <header className="header">
            <nav className="nav container">

              
             
                <a className="nav__logo" href='/'> Infinity</a>
               
            </nav>
        </header>
    </div>

    <div className='App'>
       <Redditheader subreddit = {subreddit}/>
    <div className="filters">
<Chips>
  <Chip value="new" onClick={(e) => filtered(e.target.value)}>New</Chip>
  <Chip value="best" onClick = {(e) => filtered(e.target.value)}>Top</Chip>
  <Chip value="rising" onClick = { (e) => filtered(e.target.value)}>Rising</Chip>
  <Chip value="hot" onClick = {(e) => filtered(e.target.value)}>Random</Chip> 
</Chips>
</div>

{img && img.filter(img => img.data.post_hint === 'image' || img.data.post_hint === 'hosted:video').length === 0 ? <div className="">
  <NoPost/>
    </div> :
<Masonry
  breakpointCols={breakpointColumnsObj}
  className="my-masonry-grid"
  columnClassName="my-masonry-grid_column">
   { img && img.map(imgs => imgs.data.post_hint ==='image' ? (
        <>
         <div id='center'> 


         <Link 
          key={imgs.data.id}
          to={{
            pathname: `/searchreddit/${imgs.data.id}`,
            state: { background: location }
              }}

         >  
         <LazyLoadImage src={imgs.data.url_overridden_by_dest} effect="blur" delayMethod='debounce' threshold='100' key={imgs.data.id}  >
           </LazyLoadImage  >
           </Link>   
          </div>
           </>    
       ): imgs.data.post_hint ==='hosted:video' && (
        <>

<Link 
          key={imgs.data.id}
          to={{
            pathname: `/searchreddit/${imgs.data.id}`,
            state: { background: location }
              }}
         >      
         <video playing= 'true' poster={placeit} muted playIcon url={imgs.data.media.reddit_video?.fallback_url} key={imgs.data.id} />
        </Link>

        </>
       )
       
       ) 
     }   
       </Masonry>
        }
  </div> 
     </>
   )  
}
function ImageView() {
  let { id } = useParams();
 
  const [detailphoto, setDetailphoto] = useState();
  useEffect(() => {
    fetch(`https://www.reddit.com/${id}.json`)
      .then((response) => response.json())
      .then((data) => setDetailphoto(data[0].data.children[0].data));
  }, [id]);
  
  return (
    <div>
      {<Image  color={detailphoto}  />}
     
    </div>
  );
}

function Modal() {
  let history = useHistory();
  let { id } = useParams();
  const {data} = useSWR(`https://www.reddit.com/${id}.json`, fetcher);

  if (!data) return <p className="postloader"><Spinner animation="border" variant="primary" /></p>;
  let back = (e) => {
    e.stopPropagation();
    history.goBack();
  };

  return (
    <div >
      <div className="model">
        <button class="closebutton"  onClick={back} className="closebutton">
         X
        </button>
        {
          data && data[0].data.children[0].data.post_hint ==='hosted:video' ? (
            <video controls autoPlay src={data[0].data.children[0].data.media.reddit_video.fallback_url}></video>
          ) : (
            data && data[0].data.children[0].data.post_hint ==='image' ? (
              <img src={data[0].data.children[0].data.url_overridden_by_dest} alt=""/>
            ) : ''
          )
        }

        <p className="picwithtext">{data && data[0].data.children[0].data.title}</p>
        <span className="subredditname">{data && data[0].data.children[0].data.subreddit}</span> 
        <a href={`http://www.reddit.com${data && data[0].data.children[0].data.permalink}`} className="sourcecontainer">
        <LinkCircle className='redditsource' size="30" color="#ffffff"/>
        <p className="sourcetext">Source</p>
        </a>
      </div>
    </div>
  );
}

