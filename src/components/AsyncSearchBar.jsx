import React, { useEffect, useState } from "react";
import AsyncSelect from "react-select/async";
import { SearchedPost } from "./SearchedPost";
import { Link, Navigate, useHistory } from "react-router-dom";
import { components } from "react-select";
import { useLocation } from "react-router-dom";
import {useRefresh} from 'react-tidy'
import {useStorage} from 'react-tidy'
import { Redirect } from "react-router-dom";



export const AsyncSearchBar = (props) => {
  const [inputValue, setValue] = useState("");
  const [selectedValue, setSelectedValue] = useState("");
  const [searchfetched, setSearchfetched] = useState("");
  const [token, setToken] = useStorage('token')

  const history = useHistory();
  const location = useLocation();
  let background = location.state && location.state.background.pathname;


  console.log(history);
  const refresh = useRefresh()

  const handleInputChange = (value) => {
    setValue(value);
 };



  const handleChange = (value) => {

   
    history.push({
      pathname: `/r/${value.value}`, 
      state: { background: location }
    })

    console.log(location)

  
    history.push(`/r/${value.value}`);
    window.location.reload();



  };



  const loadOptions = (inputValue) => {
    return fetch(
      `https://www.reddit.com/subreddits/search.json?q=${inputValue}&nsfw:1&limit=20`
    )
      .then((response) => response.json())
      .then((json) => {
        return json.data.children.map((child) => ({
          value: child.data.display_name,
          label: child.data,
        }));
      });
  };

  return (
    <>
      <AsyncSelect
        cacheOptions
        defaultOptions
        value={selectedValue.value}
        getOptionLabel={(e) =>
          e.label.display_name + (e.label.over18 ? " NSFW" : "")
        }
        getOptionValue={(e) => e.label.display_name}
        loadOptions={loadOptions}
        onInputChange={handleInputChange}
        onChange={handleChange}
      />
      <SearchedPost searchfetched={searchfetched} />
     
    </>
  );
};
