import "./App.css";
import { Switch, Route, Link, Router } from "react-router-dom";
import React, { useState} from "react";
import SimpleInfiniteList from "./components/Api";
import Vids from "./components/Vids";
import { Gfycat } from "./components/Gfycat";
import { Offcanvas} from 'react-bootstrap';
import { AsyncSearchBar } from "./components/AsyncSearchBar";
import SearchReddit from './components/SearchReddit';
import { Mobileheader } from "./components/Mobileheader";
import { useLocation } from "react-router-dom";
import { Searchinside } from "./components/Searchinside";
import { NavLink } from "react-router-dom";
import { TopSubs } from "./components/TopSubs";
import KUTE from 'kute.js';

function App() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
 const location = useLocation();

 const withoutnav = location.pathname
 



  return (
    <>
  

   {withoutnav.includes('/r/') ? <Searchinside handleShow={handleShow}/> : <Mobileheader handleShow={handleShow}/> } 
      
    <Offcanvas show={show} onHide={handleClose} className='searchbackground'>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Search</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
        <AsyncSearchBar handleClose= {handleClose} />
        <div>
         {/* <TopSubs/> */}

        </div>
        </Offcanvas.Body>
      </Offcanvas>
        <Switch>                                                                              
          <Route path="/videos" component={Vids} />
          <Vids/>
        </Switch>
        <Switch>
        <Route path ='/gfycat' component={Gfycat}/> 
          
        </Switch>
        <Switch>
        <Route exact path='/' component={SimpleInfiniteList} />  
        <SimpleInfiniteList />
        </Switch>

        <Switch>
        <Route path ='/r/:subreddit' component={SearchReddit}/>   
        <SearchReddit handleShow={handleShow} />
        </Switch>
    </>
  );
}

export default App;
